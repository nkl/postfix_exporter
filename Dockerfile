FROM golang:latest AS build-env
WORKDIR /build
ADD . /build
RUN apt-get update && apt-get install -y build-essential \
    git \
    libsystemd-dev
RUN cd /build && go build -o postfix_exporter

FROM debian:stretch-slim
EXPOSE 9154
RUN touch /var/log/postfix_exporter_input.log
COPY --from=build-env /build/postfix_exporter .
ENTRYPOINT ./postfix_exporter